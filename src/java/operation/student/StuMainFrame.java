/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package operation.student;

import entities.Resourceinfo;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import tools.PublicFields;
import tools.StaticFields;

/**
 *
 * @author myPC
 */
@SessionScoped
@Named
public class StuMainFrame implements Serializable {

    @Inject
    PublicFields publicFields;
    Resourceinfo resource = new Resourceinfo();
    private String pageName;

    public List<Resourceinfo> getResouceMap() {
        return publicFields.getReslistMap(StaticFields.STUDENT_ROLE);
    }

    /**
     * @return the pageName
     */
    public String getPageName() {
        if(null==pageName){
            pageName="news";
        }
        return pageName;
    }

    /**
     * @param pageName the pageName to set
     */
    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

}
