/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package operation.student;

import entities.News;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import sessionBeans.NewsFacadeLocal;
import tools.StaticFields;

@Named
@SessionScoped
public class StuNewsBean implements Serializable {

    @EJB
    NewsFacadeLocal newsFacadeLocal;
    private News news = new News();
    private List<News> recentNews;
    private News directNews;

    public String directToNews() {
        String newsId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(StaticFields.REQUEST_NEWS_ID);
        directNews = (News) (newsFacadeLocal.getQueryResultList("select * from news where id=" + newsId).get(0));
        return "news.xhtml";
    }

    /**
     * @return the news
     */
    public News getNews() {
        return news;
    }

    /**
     * @param news the news to set
     */
    public void setNews(News news) {
        this.news = news;
    }

    /**
     * @return the directNews
     */
    public News getDirectNews() {
        return directNews;
    }

    /**
     * @param directNews the directNews to set
     */
    public void setDirectNews(News directNews) {
        this.directNews = directNews;
    }

    /**
     * @return the recentNews
     */
    public List<News> getRecentNews() {
        if (null == recentNews) {
            int begin = (StaticFields.STU_NEWS_COUNT - StaticFields.NUM_NEWS_SHOWN) < 0 ? 0 : (StaticFields.STU_NEWS_COUNT - StaticFields.NUM_NEWS_SHOWN);
            recentNews = newsFacadeLocal.getQueryResultList("select * from news where for_role=" + StaticFields.STUDENT_ROLE + " OFFSET " + begin+ " ROWS");
        }
        return recentNews;
    }

    /**
     * @param recentNews the recentNews to set
     */
    public void setRecentNews(List<News> recentNews) {
        this.recentNews = recentNews;
    }
}
