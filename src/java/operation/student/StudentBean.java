/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package operation.student;

import entities.Student;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;
import sessionBeans.SchoolFacadeLocal;
import sessionBeans.StudentFacadeLocal;

/**
 *
 * @author HgsPc
 */
@Named
@SessionScoped
public class StudentBean implements java.io.Serializable {

    @EJB
    private StudentFacadeLocal studentFacadeLocal;
    @Inject
    private StuCheckLogin checkLogin;
    private int cityId1;
    private Part excelFile1;
    private List<Student> studentList;
    private String schoolId1;
    private String studentname1;
    private String schoolId;
    private int myro[];
    private Student student;

    public void setStudentList(List<Student> student) {
        this.studentList = student;
    }

    public void update() {
        studentFacadeLocal.edit(student);
    }

    public int getCityId() {
        return cityId1;
    }

    public void setCityId(int cityId) {
        this.cityId1 = cityId;
    }

    public Part getExcelFile() {
        return excelFile1;
    }

    public void setExcelFile(Part excelFile) {
        this.excelFile1 = excelFile;
    }

    /**
     * @return the student
     */
    public List<Student> getStudent() {
        if (null != this.schoolId) {
            studentList = studentFacadeLocal.getQueryResultList("select * from student where school_id ='" + schoolId + "'");
        }
        return studentList;
    }

    public void save(String uno, String schoolId) {
        this.studentFacadeLocal.executUpdate("update student set school_id='" + schoolId + "' where id=" + Integer.parseInt(uno));

    }

    public String direct2BackupImport() {
        return "importStudent";
    }

    public void deleteRow(Student user) {
        studentFacadeLocal.remove(user);
    }

    public String addStudent() {
        //Need to Do
        System.out.println("Need to do");
        return "studentInfo.xhtml";
    }

    public String addTeacher() {
        System.out.println("Need to do");
        return null;
    }

    /**
     * @return the schoolId //
     */
    public String getSchoolId() {
        return schoolId1;
    }

    /**
     * @param schoolId the schoolId to set
     */
    public void setSchoolId(String schoolId) {
        this.schoolId1 = schoolId;
    }

    /**
     * @return the studentname
     */
    public String getStudentname() {
        return studentname1;
    }

    /**
     * @param studentname the studentname to set
     */
    public void setStudentname(String studentname) {
        this.studentname1 = studentname;
    }

    /**
     * @return the classId
     */
    public String getClassId() {
        return schoolId;
    }

    /**
     * @param classId the classId to set
     */
    public void setClassId(String classId) {
        this.schoolId = classId;
    }

    /**
     * @return the ro
     */
    public int[] getRo() {
        myro = new int[]{0, 1, 2};
        return myro;
    }

    /**
     * @param ro the ro to set
     */
    public void setRo(int[] ro) {
        this.myro = ro;
    }

    /**
     * @return the roo
     */
    /**
     * @return the checkLogin
     */
    public StuCheckLogin getCheckLogin() {
        return checkLogin;
    }

    /**
     * @param checkLogin the checkLogin to set
     */
    public void setCheckLogin(StuCheckLogin checkLogin) {
        this.checkLogin = checkLogin;
    }

    /**
     * @param student the student to set
     */
    public void setStudent(Student student) {
        this.student = student;
    }

}
