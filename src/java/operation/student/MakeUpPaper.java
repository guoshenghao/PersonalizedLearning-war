/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operation.student;

import entities.Knowledge;
import entities.Leadpoint;
import entities.Question;
import entities.Student;
import entities.Testpaper;
import entities.WrongAnswerCollection;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import sessionBeans.KnowledgeFacadeLocal;
import sessionBeans.StudentFacadeLocal;
import sessionBeans.TestpaperFacadeLocal;
import sessionBeans.WrongAnswerCollectionFacadeLocal;
import tools.ga.GA4Testpaper;

/**
 * @param studentid
 * @param subjectid
 * @author duan
 */
public class MakeUpPaper {
    @EJB
    private KnowledgeFacadeLocal knowledgeFacadeLocal;
    @EJB
    private StudentFacadeLocal studentFacadeLocal;
    @EJB
    private TestpaperFacadeLocal testpaperFacadeLocal;
    @EJB
    private WrongAnswerCollectionFacadeLocal wrongAnswerCollectionFacadeLocal;
        
    public int makeUp(int studentId,int subjectId,int answeredinterval){
        List<Question> question=new ArrayList<>();
        Student student=studentFacadeLocal.find(studentId);
//        没有subjectid 无法
//        List<WrongAnswerCollection> wacs=student.getWrongAnswerCollectionList();
//        for(WrongAnswerCollection w:wacs){
//            w.getQuestionIds();
//        }
        List<Leadpoint> leadpoint=student.getLeadpointList();
        for(Leadpoint l:leadpoint){
            if(l.getKnowledgeId().equals(studentId)){
                Knowledge k =knowledgeFacadeLocal.find(l.getKnowledgeId());
                question.addAll(k.getQuestionList());
                break;
            }
        }
        GA4Testpaper gat=new GA4Testpaper();
        try {
            question=gat.getCandidateQuestions(question, 80f, 0f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return savepaper(question, student, subjectId, answeredinterval);
    }
    private int savepaper(List<Question> questions,Student student,int subjectId,int answeredinterval){
        Testpaper testpaper=new Testpaper();
        testpaper.setStudentId(student);
        String questionIds=null;
        for(Question q:questions){
            questionIds+=q.getId().toString();
        }
        testpaper.setQuestionIds(questionIds);
        testpaperFacadeLocal.create(testpaper);
        testpaper.setAnsweredInterval(answeredinterval);
        return testpaper.getId();
    }
}
