/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package operation.teacher;

import entities.TeacherAdmin;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import sessionBeans.StudentFacadeLocal;
import sessionBeans.TeacherAdminFacadeLocal;
import tools.StaticFields;

/**
 *
 * @author myPC
 */
@Named
@SessionScoped
public class TeaCheckLogin implements Serializable {

    @EJB
    private TeacherAdminFacadeLocal teacherAdminFacadeLocal;
    @EJB
    private StudentFacadeLocal studentFacadeLocal;
    private TeacherAdmin user;
   
    private String username, password;
    private String rand;
    private int tryTime = 0;
    private boolean logined = false, rendered = true;
    private final int tryTimeLimitedNumber = 3;

    public  boolean isUserlegal(String name, String password) {
        boolean result;
        List<TeacherAdmin> userList;
        userList = teacherAdminFacadeLocal.getQueryResultList("select * from teacher_admin where name='" + name + "' and password='" + password + "'");
        if (null == userList || userList.isEmpty()) {
            this.user = null;
            result = false;
        } else {
            this.user = userList.get(0);
            result = true;
        }
        return result;
    }

    public String validateUser() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        String genRand = ((String) session.getAttribute("rand")).trim();
        if (tryTime++ < tryTimeLimitedNumber) {//尝试登录未超过规定次数
            if (genRand.equals(this.rand)) {//验证码正确
                if (isUserlegal(this.username, this.password)) {//用户存在
                    session.setAttribute(StaticFields.SESSION_MYUSER, this.getUser());
                    tryTime = 0;
                    this.logined = true;
                    return "/operation/main?faces-redirect=true";

                    // return;
                } else {//用户名或密码错误
                    context.addMessage("globalMessages", new FacesMessage(StaticFields.MESSAGES_PROPTES.getString("ivalidUP")));
                    this.rand = null;
                    this.password = null;
                    return null;
                }
            } else {//验证码错误
                this.rand = null;
                this.password = null;
                context.addMessage("validatorCode", new FacesMessage(StaticFields.MESSAGES_PROPTES.getString("invValiCode")));
                return null;
            }
        } else {
            this.rendered = false;
            context.addMessage("globalMessages", new FacesMessage(StaticFields.MESSAGES_PROPTES.getString("tryFaild")));
            this.rand = null;
            this.password = null;
            return null;
        }
    }

    public String logout() {
        this.user = null;
        this.username = null;
        this.logined = false;
        this.password = null;
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.invalidate();
        return "/login/login?faces-redirect=true";
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @param rand the rand to set
     */
    public void setRand(String rand) {
        this.rand = rand;
    }

    public String getRand() {
        return this.rand;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    /**
     * @return the rendered
     */
    public boolean isLogined() {
        return logined;
    }

    /**
     * @param rendered the rendered to set
     */
    public void setLogined(boolean rendered) {
        this.logined = rendered;
    }

    /**
     * @return the rendered
     */
    public boolean isRendered() {
        return rendered;
    }

    /**
     * @param rendered the rendered to set
     */
    public void setRendered(boolean rendered) {
        this.rendered = rendered;
    }

    /**
     * @return the user
     */
    public TeacherAdmin getUser() {
        return user;
    }

    public void updateUser(TeacherAdmin user) {
        teacherAdminFacadeLocal.edit((TeacherAdmin) user);
    }
}
