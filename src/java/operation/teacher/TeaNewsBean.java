/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package operation.teacher;

import entities.News;
import entities.TeacherAdmin;
import java.io.Serializable;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import sessionBeans.NewsFacadeLocal;
import tools.StaticFields;

@Named
@SessionScoped
public class TeaNewsBean implements Serializable {

    @EJB
    NewsFacadeLocal newsFacadeLocal;
    @Inject
    private TeaCheckLogin checkLogin;
    private News news = new News();
    private List<News> recentNews;
    private LinkedHashMap<String, List<News>> recentNewsMap;
    private News directNews;

    @PostConstruct
    public void init() {
        StaticFields.TEA_NEWS_COUNT=newsFacadeLocal.getQueryCount("select count(*) from new where FOR_ROLE="+StaticFields.TEACHER_ROLE);
        StaticFields.STU_NEWS_COUNT=newsFacadeLocal.getQueryCount("select count(*) from new where FOR_ROLE="+StaticFields.STUDENT_ROLE);
    }

    public String directToNews() {
        String newsId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(StaticFields.REQUEST_NEWS_ID);
        directNews = (News) (newsFacadeLocal.getQueryResultList("select * from news where id=" + newsId).get(0));
        return "news.xhtml";
    }

    public String addNews() {
        TeacherAdmin myUser = getCheckLogin().getUser();
        Calendar temCa = Calendar.getInstance();
        int month = temCa.get(Calendar.MONTH);
        String temMonth = month < 10 ? "0" + month : "" + month;
        String myData = temCa.get(Calendar.YEAR) + "-" + temMonth.trim() + "-" + temCa.get(Calendar.DAY_OF_MONTH);
        if (this.news.getContent().trim().length() >= 0) {
            String sqlString = "insert into news (content, inputDate, teacherno , newstitle) values('"
                    + this.news.getContent().trim() + "','"
                    + myData + "', '"
                    + myUser.getId() + "','"
                    + this.news.getNewstitle().trim() + "')";
            newsFacadeLocal.executUpdate(sqlString);
            this.news = new News();
            //需要调整Map中的list
        } else {
            FacesContext.getCurrentInstance().addMessage("latestMessage", new FacesMessage("您需要加入消息的内容，而不能为空"));
        }
        return null;
    }

    public String delete(int id) {
        newsFacadeLocal.executUpdate("delete from news where id=" + id);
        return null;
    }

    /**
     * @return the news
     */
    public News getNews() {
        return news;
    }

    /**
     * @param news the news to set
     */
    public void setNews(News news) {
        this.news = news;
    }

    /**
     * @return the checkLogin
     */
    public TeaCheckLogin getCheckLogin() {
        return checkLogin;
    }

    /**
     * @param checkLogin the checkLogin to set
     */
    public void setCheckLogin(TeaCheckLogin checkLogin) {
        this.checkLogin = checkLogin;
    }

    /**
     * @return the directNews
     */
    public News getDirectNews() {
        return directNews;
    }

    /**
     * @param directNews the directNews to set
     */
    public void setDirectNews(News directNews) {
        this.directNews = directNews;
    }

    /**
     * @return the recentNews
     */
    public List<News> getRecentNews() {
        if (null == recentNews) {
            int begin = (StaticFields.TEA_NEWS_COUNT - StaticFields.NUM_NEWS_SHOWN) < 0 ? 0 : (StaticFields.TEA_NEWS_COUNT - StaticFields.NUM_NEWS_SHOWN);
            recentNews = newsFacadeLocal.getQueryResultList("select * from news where for_role="+StaticFields.TEACHER_ROLE+" OFFSET "+begin+ " ROWS");
        }
        return recentNews;
    }

    /**
     * @param recentNews the recentNews to set
     */
    public void setRecentNews(List<News> recentNews) {
        this.recentNews = recentNews;
    }
}
