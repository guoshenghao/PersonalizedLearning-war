/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backing.beans;

import entities.Knowledge;
import entities.Leadpoint;
import entities.Major;
import entities.Student;
import entities.Subject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.function.Consumer;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import sessionBeans.KnowledgeFacadeLocal;
import sessionBeans.LeadpointFacadeLocal;
import sessionBeans.MajorFacadeLocal;
import sessionBeans.SubjectFacadeLocal;
import tools.PublicFields;
import tools.StaticFields;


/**
 *
 * @author lenovo
 */
@Named(value = "initKnowledgeFront")
@SessionScoped
public class initKnowledgeFront implements Serializable{

    /**
     * Creates a new instance of initKnowledgeFront
     */
    
    @EJB 
    private MajorFacadeLocal majorFacadeLocal;
    @EJB
    private SubjectFacadeLocal subjectFacadeLocal;
    @EJB
    private KnowledgeFacadeLocal knowledgeFacadeLocal;
    @EJB
    private LeadpointFacadeLocal leadpointFacadeLocal;
    @Inject 
    private PublicFields publicFields;
    //年级&专业，只读
    private List<SelectItem> majorItems=new ArrayList<>();
    //课程，只读
    private List<SelectItem> subjectItems=new ArrayList<>();
    
    private List<SelectItem> knowledgeItems=new ArrayList<>();
    private Integer knowledge=0;
    //某个年级&专业，读写
    private Integer major=0;
    //某门课程，读写
    private Integer subject=0;
    //某门课程的所有知识点，读写
    private List<Knowledge> knowledges=new ArrayList<>();
    //前沿知识点,
    private List<Knowledge> knowledges1=new ArrayList<>();
    //某个学生的某门课程的知识点前沿，读写
    private List<Leadpoint> leadpoints=new ArrayList<>();
    private final HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);


    
    public List<SelectItem> getMajorItems() {
        if(null==majorItems || majorItems.isEmpty()){
            /* 从数据库中获取所有年级及专业 */
            List<Major> majors=majorFacadeLocal.findAll();
            /*往列表项中添加默认项*/
            majorItems.add(new SelectItem(tools.StaticFields.MESSAGES_PROPTES.getString("pleaseSelect"))); 
            /*遍历获取的年级及专业，并将其id和name分别作为列表项的key,value*/
            for (Major m : majors) {
                majorItems.add(new SelectItem(m.getId(), m.getName()));
            }    
        }
        return majorItems;
    }

    public List<SelectItem> getSubjectItems() {
        /*首先情况课程列表项，以避免累加*/
        subjectItems.clear();
        subjectItems.add(new SelectItem(tools.StaticFields.MESSAGES_PROPTES.getString("pleaseSelect")));
        /*从专业和课程的Map中取出用户所选专业的所有课程*/
        List<Subject> subjects= publicFields.getMajorSubjectMap().get(major);
        /*如果所选年级或专业没有对应的课程，则直接返回空列表*/
        if(null == subjects || subjects.isEmpty()){
            return subjectItems;
        }
        /* 如果有对应的课程，则遍历将id和name分别作为列表项的key,value */
        else{
           
            for (Subject sub : subjects) {
                subjectItems.add(new SelectItem(sub.getId(), sub.getName()));
            }  
        }
        return subjectItems;
    }
    
        
    public List<SelectItem> getKnowledgeItems() {
         /*首先清空课程列表项，以避免累加*/
        knowledgeItems.clear();
        knowledgeItems.add(new SelectItem(tools.StaticFields.MESSAGES_PROPTES.getString("pleaseSelect")));
        /*从专业和课程的Map中取出用户所选专业的所有课程*/
        List<Knowledge> ks= publicFields.getSubKnowleMap().get(subject);
        /*如果所选年级或专业没有对应的课程，则直接返回空列表*/
        if(null == ks || ks.isEmpty()){
            return knowledgeItems;
        }
        /* 如果有对应的课程，则遍历将id和name分别作为列表项的key,value */
        else{
            for (Knowledge k : ks) {
                knowledgeItems.add(new SelectItem(k.getId(),k.getName()));
            }  
        }
        return knowledgeItems;
    }

    public Integer getMajor() {
        return major;
    }

    public void setMajor(Integer major) {
        this.major = major;
    }

    public Integer getSubject() {
        return subject;
    }

    public void setSubject(Integer subject) {
        this.subject = subject;
    }
    
    public List<Knowledge> getKnowledges() {
        /*从课程和知识点的Map中获取所选课程的所有知识点，用于绘制知识点图谱*/
        knowledges = publicFields.getSubKnowleMap().get(subject);
        return knowledges;
    }

    public void setKnowledges(List<Knowledge> knowledges) {
        this.knowledges = knowledges;
    }

    public Integer getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(Integer knowledge) {
        this.knowledge = knowledge;
    }
    
    
    
    
    public List<Leadpoint> getLeadpoints() {
        /*获取当前用户*/
       Student student=(Student)(session).getAttribute(StaticFields.STUDENTREF);
       String knowledgeId=null;
       for(int i=3;i<10;i++){
           knowledges1.add(knowledgeFacadeLocal.find(i));
       }
       List<Knowledge> leadKnowledges=knowledges1;
        /*将用户所选知识点中不满足知识点前沿的性质的知识点剔除*/
        knowledges1.stream().map((k) -> k.getKnowledgeList()).forEachOrdered(new Consumer<List<Knowledge>>() {
           @Override
           public void accept(List<Knowledge> ks) {
               //knowledgeId=getPreNode(k);
               knowledges1.stream().filter((k1) -> (ks.contains(k1))).forEachOrdered((k1) -> {
                   leadKnowledges.remove(k1);
               });
           }
       });
        knowledgeId = leadKnowledges.stream().map((k) -> k.getId()+tools.StaticFields.DELIMITED).reduce(knowledgeId, String::concat); /*将满足知识点前沿条件的知识点的id连接成字符串*/
       Leadpoint leadpoint=new Leadpoint();    
       leadpoint.setId(0);
       leadpoint.setKnowledgeId(knowledgeId);
       leadpoint.setCreateDate(Calendar.getInstance().getTime());
       String sql = "select * from leadpoint where student_id =" + student.getId() + "and subject_id=" + subject;
       /* 如果数据库中不存在该学生的该课程的知识点前沿，那么添加数据 */
       if(null==leadpointFacadeLocal.getQueryResultList(sql) || leadpointFacadeLocal.getQueryResultList(sql).isEmpty()){
           leadpointFacadeLocal.create(leadpoint);
       }
       /*否则修改数据*/
       else{
           leadpointFacadeLocal.edit(leadpoint);
       }
       leadpoints=leadpointFacadeLocal.getQueryResultList(sql);
       return leadpoints;
    }

    public void setLeadpoints(List<Leadpoint> leadpoints) {
        this.leadpoints = leadpoints;
    }
    
}
