/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backing.beans;

import entities.Resourceinfo;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import tools.PublicFields;

/**
 *
 * @author asus
 */
@SessionScoped
@Named
public class MyMainFrame implements Serializable {

    @Inject
    PublicFields publicFields;
    @Inject
            CheckLogin checkLogin;
    Resourceinfo resource = new Resourceinfo();
    private String pageName;

    public List<Resourceinfo> getResouceMap() {
        return publicFields.getReslistMap(checkLogin.getRoleType());
    }

    /**
     * @return the pageName
     */
    public String getPageName() {
        if(null==pageName){
            pageName="news";
        }
        return pageName;
    }

    /**
     * @param pageName the pageName to set
     */
    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

}
