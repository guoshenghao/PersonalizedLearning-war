/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package commonBackingBean;

import entities.School;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import sessionBeans.SchoolFacadeLocal;
import tools.StaticFields;

/**
 *
 * @author myPC
 */
@Named
@SessionScoped
public class SchoolBean implements Serializable {

    @EJB
    private SchoolFacadeLocal schoolFacadeLocal;
    private School school;
    private List<School> schoolList;
    private String schoolId;
    private String schoolName;
    private String pinyin;
    private String mytype;
    private LinkedHashMap<String, Integer> myCateType;

    public String alterSchool(String sId, String sName, String sPinyin) {
        schoolFacadeLocal.executUpdate("update school set name='" + sName + "',pinyin='" + sPinyin + " where id=" + sId);
        return "viewSchools.xhtml";
    }

    public String addSchool() {
        schoolFacadeLocal.executUpdate("insert into school" + " ( name, pinyin) values('" +  schoolName + "', '" + pinyin + "')");
        return "viewSchools.xhtml";
    }

    public LinkedHashMap<String, Integer> getMytypeMap() {
        if (null == myCateType) {
            myCateType = new LinkedHashMap<>();
            myCateType.put(StaticFields.CATE_TYPE[0], 0);
            myCateType.put(StaticFields.CATE_TYPE[1], 1);
            myCateType.put(StaticFields.CATE_TYPE[2], 2);
        }
        return myCateType;
    }

    /**
     * @return the nameofunit
     */
    public School getSchool() {
        return school;
    }

    /**
     * @param schoolPara the nameofunit to set
     */
    public void setSchool(School schoolPara) {
        this.school = schoolPara;
    }

    /**
     * @return the nameofunitList
     */
    public List<School> getNameofunitList() {
        if (schoolList == null) {
            schoolList = schoolFacadeLocal.findAll();
        }
        return schoolList;
    }

    /**
     * @param nameofunitList the nameofunitList to set
     */
    public void setSchoolList(List<School> nameofunitList) {
        this.schoolList = nameofunitList;
    }

    /**
     * @return the schoolId
     */
    public String getSchoolId() {
        return schoolId;
    }

    /**
     * @param schoolId the schoolId to set
     */
    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    /**
     * @return the schoolName
     */
    public String getSchoolName() {
        return schoolName;
    }

    /**
     * @param schoolName the schoolName to set
     */
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    /**
     * @return the pinyin
     */
    public String getPinyin() {
        return pinyin;
    }

    /**
     * @param pinyin the pinyin to set
     */
    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

       /**
     * @return the mytype
     */
    public String getMytype() {
        return mytype;
    }

    /**
     * @param mytype the mytype to set
     */
    public void setMytype(String mytype) {
        this.mytype = mytype;
    }
}
