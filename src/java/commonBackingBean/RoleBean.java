/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonBackingBean;

import entities.Roleinfo;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import sessionBeans.RoleinfoFacadeLocal;

/**
 *
 * @author asus
 */
@Named
@ApplicationScoped
public class RoleBean implements java.io.Serializable{
    @EJB
    private RoleinfoFacadeLocal roleinfoFacadeLocal;
    
    public Roleinfo getRoleinfo(int id){
        return roleinfoFacadeLocal.find(id);
    }
}
