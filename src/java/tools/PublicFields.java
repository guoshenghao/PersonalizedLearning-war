/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import entities.City;
import entities.Knowledge;
import entities.Major;
import entities.Resourceinfo;
import entities.Roleinfo;
import entities.Subject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import sessionBeans.CityFacadeLocal;
import sessionBeans.KnowledgeFacadeLocal;
import sessionBeans.MajorFacadeLocal;
import sessionBeans.ResourceinfoFacadeLocal;
import sessionBeans.RoleinfoFacadeLocal;
import sessionBeans.SubjectFacadeLocal;

/**
 *
 * @author Idea
 */
@Named
@ApplicationScoped
public class PublicFields implements java.io.Serializable {

    @EJB
    private CityFacadeLocal cityFacadeLocal;
    @EJB
    private RoleinfoFacadeLocal roleinfoFacadeLocal;
    @EJB
    private ResourceinfoFacadeLocal resourceinfoFacadeLocal;
    @EJB
    private MajorFacadeLocal majorFacadeLocal;
    @EJB
    private SubjectFacadeLocal subjectFacadeLocal;
    @EJB
    private KnowledgeFacadeLocal knowledgeFacadeLocal;

    private final Calendar c = Calendar.getInstance();
    private final int year = c.get(Calendar.YEAR), currentMonth = c.get(Calendar.MONTH);
    private int month = c.get(Calendar.MONTH);
    private LinkedHashMap<Integer, Integer> yearMap;
    private LinkedHashMap<Integer, Integer> monthMap;
    private final LinkedHashMap<Integer, Integer> dayMap = new LinkedHashMap<>();
    private List<String> unitIdList = new ArrayList<>();
    //private LinkedHashMap<Integer, HashMap<Resourceinfo, List<Resourceinfo>>> resourceslistMap;//每个角色对应的功能菜单
    private HashMap<Integer, List<Resourceinfo>> roleResouceMap = new HashMap<>();
    private LinkedHashMap<String, Integer> roleMap = null;
    private LinkedHashMap<String, Integer> provinceMap = null;
    

    private void calcuRoleResourceList() {

        List<Roleinfo> roleList = roleinfoFacadeLocal.findAll();
        roleList.forEach((Roleinfo role) -> {
            roleResouceMap.put(role.getId(), new LinkedList<>());
        });
        List<Resourceinfo> resourceinfoList = resourceinfoFacadeLocal.getQueryResultList("select * from resourceinfo order by menuorder asc");
        roleList.forEach((Roleinfo role) -> {
            List resoursIds = Arrays.asList(role.getResources().split(","));
            resourceinfoList.forEach((Resourceinfo resourceinfo) -> {
                if (resoursIds.contains(resourceinfo.getId().toString().trim())) {
                    roleResouceMap.get(role.getId()).add(resourceinfo);
                }
            });

        });
    }

    /**
     * @param roleType
     * @return the ReslistMap
     */
    public List<Resourceinfo> getReslistMap(Integer roleType) {
        if (null == roleResouceMap || roleResouceMap.isEmpty()) {
            roleResouceMap = new HashMap<>();
            calcuRoleResourceList();
        }
        return roleResouceMap.get(roleType);
    }

    /**
     * @return the yearMap
     */
    public LinkedHashMap<Integer, Integer> getYearMap() {
        if (null == yearMap) {
            yearMap = new LinkedHashMap<>();
            yearMap.put(c.get(Calendar.YEAR), c.get(Calendar.YEAR));
            yearMap.put(c.get(Calendar.YEAR) - 1, c.get(Calendar.YEAR) - 1);
        }
        return yearMap;
    }

    public LinkedHashMap<Integer, Integer> getMonthMap() {
        if (null == monthMap || monthMap.isEmpty()) {
            monthMap = new LinkedHashMap<>();
            for (int i = 0; i < 12; i++) {
                monthMap.put(i + 1, i);
            }
        }
        return monthMap;
    }

    public LinkedHashMap<Integer, Integer> getDayMap() {
        dayMap.clear();
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.YEAR, year - c.get(Calendar.YEAR));
        c1.add(Calendar.MONTH, month - currentMonth + 1);
        for (int i = 0; i < c1.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            dayMap.put(i + 1, i + 1);
        }
        return dayMap;
    }

    /**
     * @return the month
     */
    public int getMonth() {
        return month;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(int month) {
        this.month = month;
    }

    /**
     * @return the roleMap
     */
    public LinkedHashMap<String, Integer> getRoleMap() {
        if (null == this.roleMap || this.roleMap.isEmpty()) {
            List<Roleinfo> role = roleinfoFacadeLocal.findAll();
            if (null != role && role.size() > 0) {
                Iterator<Roleinfo> it = role.iterator();
                roleMap = new LinkedHashMap<>();
                while (it.hasNext()) {
                    Roleinfo ro1 = it.next();
                    this.roleMap.put(ro1.getRolename(), ro1.getId());
                }
            }
        }
        return roleMap;
    }

    /**
     * @return the provinceList
     */
    public LinkedHashMap<String, Integer> getProvinceMap() {
        if (null == provinceMap || provinceMap.size() == 0) {
            List<City> cityList = cityFacadeLocal.findAll();
            for (City city : cityList) {
                if (null != city.getProvince()) {
                    provinceMap.put(city.getName(), city.getId());
                }
            }
        }
        return provinceMap;
    }
    /**
     * 获取专业和课程的键值对，其中课程是list类型
     * @return the majorsubjectMap
     */
    public LinkedHashMap<Integer,List<Subject>> getMajorSubjectMap(){
        /*首先获取所有专业*/
        List<Major> majors=majorFacadeLocal.findAll();      
        LinkedHashMap<Integer,List<Subject>> majorSubjectMap=new LinkedHashMap<Integer, List<Subject>>();
        if(null==majorSubjectMap || majorSubjectMap.isEmpty()){
            /*遍历年级及专业，按其将课程选择出来并存入Map中*/
            for (Major major : majors) {
                Integer majorId=major.getId();
                String sql="select * from subject where majorid="+majorId;
                majorSubjectMap.put(major.getId(), subjectFacadeLocal.getQueryResultList(sql));
            }
        }
        return majorSubjectMap;
    }
    
    /**
     * 获取课程和知识点的键值对，知识点时list类型
     * @return the subKnowleMap
     */
    public LinkedHashMap<Integer,List<Knowledge>> getSubKnowleMap(){
        /*首先获取所有课程*/
        List<Subject> subjects=subjectFacadeLocal.findAll();
        LinkedHashMap<Integer,List<Knowledge>> subKnowledgeMap=new LinkedHashMap<Integer, List<Knowledge>>();
        if(null==subKnowledgeMap || subKnowledgeMap.isEmpty()){
            /*遍历课程，将知识点按课程选择出来并存入map中*/
            for (Subject subject : subjects) {
                String sql="select * from knowledge where subject_id="+ subject.getId();
                subKnowledgeMap.put(subject.getId(), knowledgeFacadeLocal.getQueryResultList(sql));
            }
        }
        return subKnowledgeMap;
    }
    //    private  void calcuRoleResourceList1() {//带parentId的
//        //权限菜单准备完成
//        List<Resourceinfo> resourceinfoList = resourceinfoFacadeLocal.getQueryResultList("select * from resourceinfo order by parentid desc,menuorder ");//把null的排在最前面
//        HashMap<Resourceinfo, List<Resourceinfo>> resourceHashMap = new LinkedHashMap<>();
//        HashMap<Integer, Resourceinfo> parentResource = new LinkedHashMap<>();
//        resourceinfoList.forEach((Resourceinfo resourceinfo) -> {
//            if (null == resourceinfo.getParentid()) {
//                resourceHashMap.put(resourceinfo, new LinkedList<>());
//                parentResource.put(resourceinfo.getId(), resourceinfo);
//            } else {
//                resourceHashMap.get(resourceinfo.getParentid()).add(resourceinfo);
//            }
//        });
//        //获得角色种类，把每个角色的一个List放到Map里作为一个元素
//        List<Roleinfo> roleList = roleinfoFacadeLocal.findAll();
//        resourceslistMap = new LinkedHashMap<>();
//        roleList.forEach((Roleinfo role) -> {            
//            Arrays.asList(role.getResources().split(",")).forEach((String resourceId) -> {
//                HashMap<Resourceinfo, List<Resourceinfo>> temResourceHashMap = new LinkedHashMap<>();
//                Resourceinfo parent = parentResource.get(Integer.valueOf(resourceId));
//                temResourceHashMap.put(parent, resourceHashMap.get(parent));
//                resourceslistMap.getOrDefault(role.getId(), temResourceHashMap);
//            });
//        });
//    }
}
