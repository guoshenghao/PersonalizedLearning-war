/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.util.ResourceBundle;

/**
 *
 * @author Idea
 */
public class StaticFields {

    public static final int TEACHER_ROLE = 1, ADMIN_ROLE = 2, STUDENT_ROLE = 3;
    public static final int NUM_NEWS_SHOWN = 10;
    public static final String[] CATE_TYPE = new String[]{"文科", "理科", "工科"};
    public static final String LONGIN_PATH = "/login/login?faces-redirect=true";
    public static final String REGISTER_PATH="/noLogin/register?faces-redirect=true";
    public static final String SESSION_MYUSER = "myUser";
    public static final String REQUEST_NEWS_ID = "newsId";
    public static final String SESSION_SCHOOL_ID = "schoolID";//当导入某个学校的学生信息时，先把该学校加入，并获得ID值，从而在从Excel中导入学生信息时，把该ID对应的学校对象作为其内容，填到数据库中。 
    public static int STU_NEWS_COUNT = 0, TEA_NEWS_COUNT = 0;//非final，所以当有新闻加入时，其值可以发生改变。 
    public static final ResourceBundle MESSAGES_PROPTES = ResourceBundle.getBundle("messages");
    public static final String LOGIN_ROLE = "login_role";
    public static final String MAIN_PAGE = "/operation/main?faces-redirect=true";
    public static final String NOLOGIN_MAIN_PAGE="/noLogin/main?faces-redirect=true";
    public static final String STUDENTREF = "student";
    public static final String TEACHERREF = "teacher";
    public static final String DELIMITED ="&$#%@%$@#&%";
}
