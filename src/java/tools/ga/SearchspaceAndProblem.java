/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools.ga;

import entities.Question;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author asus
 */
public class SearchspaceAndProblem {

    List<Question> questionList;
    HashMap<Integer, Question> questionMap;
    private float targetScore=0, hardIndex=0;

    public void init(List<Question> questions) {
        this.questionList=questions;
        questionMap = new HashMap<>();
        Iterator it = questionList.iterator();
        int i = 0;
        while (it.hasNext()) {
            Question q = (Question) it.next();
            questionMap.put(i++, q);
        }
    }

    public float evaluate(Individual individual) {
        float sum = 0,hardSum=0,count=0;
        Boolean[] tem = individual.getX();
        for (int i = 0; i < tem.length; i++) {
            if (tem[i]) {
                sum += questionMap.get(i).getScore();
                hardSum+=questionMap.get(i).getDegree();
                count++;
            }
        }
        return Math.abs(sum-targetScore)+Math.abs(hardSum*100f/count-hardIndex*100);
    }

    /**
     * @param targetScore the targetScore to set
     */
    public void setTargetScore(float targetScore) {
        this.targetScore = targetScore;
    }

    /**
     * @param hardIndex the hardIndex to set
     */
    public void setHardIndex(float hardIndex) {
        this.hardIndex = hardIndex;
    }
}
