package tools.ga;

import java.util.Random;

public class OnePointCross {

    private int CrossNumber;
    private boolean isOdd;
    private Random r = new Random();
    private Individual[] populationAfterCrossover;

    public OnePointCross(Individual[] population, float crossProbability) throws CloneNotSupportedException {
        populationAfterCrossover = new Individual[population.length];
        this.CrossNumber = population.length / 2;
        if (population.length % 2 == 0) {
            this.isOdd = false;
        } else {
            this.isOdd = true;
        } 
        Individual[] temPopulation=null;
        for (int i = 0; i < this.CrossNumber; i++) {
            
            int tem1 = (int) (r.nextFloat() * population.length);
            int tem2 = (int) (r.nextFloat() * population.length);
            if (r.nextFloat() <= crossProbability) {
                temPopulation = population[tem1].crossover(population[tem2]);

            } else {
                temPopulation = new Individual[]{population[tem1], population[tem2]};
            }
            System.arraycopy(temPopulation, 0, populationAfterCrossover, 2 * i, temPopulation.length);
        }
        if (this.isOdd) {
            int tem1 = (int) (r.nextFloat() * population.length);
            int tem2 = (int) (r.nextFloat() * population.length);
            temPopulation = population[tem1].crossover(population[tem2]);
           populationAfterCrossover[population.length-1]=temPopulation[0];
        }
    }

    /**
     * @return the populationAfterCrossover
     */
    public Individual[] getPopulationAfterCrossover() {
        return populationAfterCrossover;
    }
}
