/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools.ga;

import entities.Question;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author asus
 */
public class GA4Testpaper {

    private SearchspaceAndProblem searchSpace=new SearchspaceAndProblem();
    private Individual[] population = new Individual[50];
    private final int mutationPointNum = 2;
    private final float crossoverProbability = 0.9f;
    private final float mutationProbability = 0.1f;
    Random random = new Random();

    public List<Question> getCandidateQuestions(List<Question> questions,float targetScore,float hardIndex) throws CloneNotSupportedException {
        searchSpace.init(questions);
        searchSpace.setHardIndex(hardIndex);
        searchSpace.setTargetScore(targetScore);
        List<Question> result = new LinkedList<>();
        //Init the population
        for (int i = 0; i < population.length; i++) {
            population[i] = new Individual();
            population[i].setSearchspaceAndProblem(this.searchSpace);
        }
        //Carry out evolution
        for (int i = 0; i < 100; i++) {
            Arrays.sort(population);
            Individual[] populationBeforeCrossover = new GeneralProportionSelection(population).getPopulationAfterSelection();
            //crossover
            Individual[] populationBeforeMutation = new OnePointCross(populationBeforeCrossover, crossoverProbability).getPopulationAfterCrossover();
            //mutation
            for (Individual populationBeforeMutation1 : populationBeforeMutation) {
                if (random.nextFloat() < mutationProbability) {
                    populationBeforeMutation1.mutate(mutationPointNum);
                }
            }
            this.population=populationBeforeMutation;
        }
        return result;
    }

}
